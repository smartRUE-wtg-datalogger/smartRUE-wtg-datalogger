Design of a low cost open hardware wind turbine generator data logger. smartRUE 
lab Smart grids Research Unit of the Electrical and Computer Engineering School-NTUA.

The wtg open hardware datalogger project, is developed at smartRUE lab, aiming 
to create all the necessary modules in order to create a datalogger compliant 
with IEC 61400-12-1 specification (Power performance measurements of electricity 
producing wind turbines). A reduced set of the datalogger building blocks can be
combined in order to build a basic datalogging setup with minimal cost, for low 
cost, diy applications such as rural electrification development projects.
